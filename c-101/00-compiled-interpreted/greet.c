#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void greet(char *name)
{
    printf("Hello, %s!\n", name);
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("Usage: ./greet <name>\n");
        exit(1);
    }

    char name[64];
    strncpy(name, argv[1], 64);
    greet(name);

    return 0;
}
