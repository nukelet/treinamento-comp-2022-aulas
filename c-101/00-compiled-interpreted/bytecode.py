import dis

def greet(name: str) -> None:
    print(f"Hello, {name}!")

def main():
    dis.dis(greet)

if __name__ == "__main__":
    main()
