#include <stdio.h>
#include <stdint.h>

/*
 * Printa os valores de um array de inteiros.
 *
 * @param[in] arr: um array de inteiros
 * @param[in] size: tamanho do array
 */
void print_int_array(int *arr, size_t size)
{
    while (size--) {
        printf("%d ", *arr);
        arr++;
    }

    printf("\n");
}

int main()
{
    int arr[5] = {1, 2, 3, 4, 5};
    print_int_array(arr, 5);

    return 0;
}
