#include <stdio.h>

int main()
{
    float a = 2;
    float b = 3;
    printf("a = %f, b = %f\n", a, b);
    printf("a + b = %f\n", a + b);
    printf("a - b = %f\n", a - b);
    printf("a * b = %f\n", a * b);
    printf("a / b = %f\n", a / b);
}
