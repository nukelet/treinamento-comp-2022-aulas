#include <stdio.h>

int main()
{
    char c = 'a';
    printf("caractere: %c\n", c);
    printf("valor numerico: %d\n", c);

    printf("caracteres: a, b, c, d, e\n");
    printf("valores numericos: %d, %d, %d, %d, %d\n",
        'a', 'b', 'c', 'd', 'e');

    return 0;
}
